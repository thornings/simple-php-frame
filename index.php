<!DOCTYPE html>


<!-- load php stuff -->
<?php

?>


<html>
    <head>
        <meta charset="UTF-8">
        <!-- STYLESHEETS -->
        <link rel="stylesheet"  type="text/css" media="screen" href="style/style.css" />

        <!-- FONTS -->
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Montsserat">

        <!-- LIBRARIES -->
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
        <!-- font awesome -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <!-- OWN SCRIPTS -->
        <script type="text/javascript" src="javascript/script.js"></script>
        <title>Thornings 2015 - a amazing Journey</title>
    </head>
    <body>
    <!-- NAVIGATION -->
        <div class="" id="nav">
            <ul id="menu">
                <li>Test</li>
                <li>Test2</li>
                <li>Test3</li>
                <li>Test4</li>
            </ul>
        </div>

  
        <i class="fa fa-cog fa-spin fa-5x"></i>
      
  
    
        <!-- CONTENT -->
        <div id="content">
            
            <h1 class="text-success"><span class="fa fa-bed fa-5x"></span> headline </h1>
            <h3 class=""><span class=""></span> hej med dig </h1>
            
            <p> lorem
            </p>
        </div>
        
    </body>
</html>
